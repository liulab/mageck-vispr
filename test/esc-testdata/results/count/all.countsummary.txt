File	Label	Reads	Mapped	Percentage	TotalsgRNAs	Zerocounts	GiniIndex	NegSelQC	NegSelQCPval	NegSelQCPvalPermutation	NegSelQCPvalPermutationFDR	NegSelQCGene
reads/ERR377000.subsample.fastq	esc2	100000	89175	0.8918	87437	45351	0.6177	0	1	1	1	0.0
reads/ERR376999.subsample.fastq	esc1	100000	88887	0.8889	87437	45017	0.6139	0	1	1	1	0.0
reads/ERR376998.subsample.fastq	plasmid	100000	89828	0.8983	87437	40817	0.5695	0	1	1	1	0.0
